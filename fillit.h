/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 14:44:02 by rvievill          #+#    #+#             */
/*   Updated: 2016/01/19 18:09:52 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define BUF_SIZE 22

# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include "libft/libft.h"

typedef struct		s_form
{
	int				tetris[8];
	char			fix;
	char			letter;
	int				last_pos;
	struct s_form	*next;
}					t_form;

typedef struct		s_limit
{
	int				c;
	int				l;
	int				square;
}					t_limit;

int					recover(int fd, int *ret);
int					error(char *str);
void				lst_new(t_form *start, char *str, int *n, char letter);
void				first_elem(t_form *new_elem);
void				ft_solving(t_form *lst, int number);
int					ft_fill(char grid[12][13], t_form *lst, t_limit pos,
		t_form *begin);
void				ft_clear(char grid[12][13], t_form *lst, t_limit pos);

#endif
