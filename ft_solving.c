/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solving.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsoudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 18:47:58 by jsoudier          #+#    #+#             */
/*   Updated: 2016/01/22 18:49:16 by jsoudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_blank(char grid[12][13])
{
	int	i;
	int	j;

	i = 0;
	while (i < 12)
	{
		j = 0;
		while (j < 12)
		{
			grid[i][j] = '.';
			j++;
		}
		grid[i][j] = '\0';
		i++;
	}
}

void		ft_print(char grid[12][13], t_limit pos)
{
	int	i;
	int	j;

	i = 0;
	while (i < pos.square)
	{
		j = 0;
		while (j < pos.square)
		{
			ft_putchar(grid[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void		ft_clear(char grid[12][13], t_form *lst, t_limit pos)
{
	int i;
	int j;

	i = 0;
	while (i < pos.square)
	{
		j = 0;
		while (j < pos.square)
		{
			if (grid[i][j] == lst->letter)
				grid[i][j] = '.';
			j++;
		}
		i++;
	}
}

void		ft_solving(t_form *lst, int number)
{
	char	grid[12][13];
	t_limit	pos;
	t_form	*begin;

	begin = lst;
	pos.c = 0;
	pos.l = 0;
	pos.square = (2 * ft_square_root(number)) + 1;
	ft_blank(grid);
	lst = begin;
	pos.square = ft_fill(grid, lst, pos, begin);
	ft_print(grid, pos);
}
