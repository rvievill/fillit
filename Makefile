# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rvievill <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/18 15:56:08 by rvievill          #+#    #+#              #
#    Updated: 2016/01/19 17:40:48 by rvievill         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit

O_SRC = main.o error.o filling_struct.o ft_solving.o ft_fill.o

FLAG = -Wextra -Wall -Werror

INC = ./libft

all: $(NAME)

%.o: %.c
	gcc $(FLAG) -c $< -I $(INC)

$(NAME): $(O_SRC)
	make -C libft
	gcc $(FLAG) -Llibft -lft -I $(INC) -o $(NAME) $(O_SRC)

clean:
	rm -rf $(O_SRC)

fclean: clean
	rm -rf $(NAME)
	make -C libft fclean

re: clean fclean all

.PHONY: all clean fclean re
