/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filling_struct.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 16:54:15 by rvievill          #+#    #+#             */
/*   Updated: 2016/01/22 16:25:03 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			first_elem(t_form *new_elem)
{
	new_elem->tetris[0] = -1;
	new_elem->fix = 0;
	new_elem->next = NULL;
}

static void		filling_tab(char *str, t_form *start, char letter)
{
	int			c[4];

	ft_bzero(c, 16);
	while (str[c[0]] != letter)
		c[0]++;
	while (str[c[1]] != '\n' || (c[0] - c[1]) > 4)
		c[1]++;
	start->tetris[1] = c[0] - c[1] - 1;
	c[2] = 1;
	while (str[++c[0]])
	{
		if (str[c[0]] == '\n')
		{
			c[1] += 5;
			c[3]++;
		}
		else if (str[c[0]] == letter)
		{
			start->tetris[++c[2]] = c[3];
			start->tetris[++c[2]] = (c[0] - c[1] - 1) - start->tetris[1];
		}
	}
	start->tetris[1] = 0;
}

static void		filling_struct(t_form *curent, char *str, char letter)
{
	curent->tetris[0] = 0;
	curent->tetris[1] = 0;
	filling_tab(str, curent, --letter);
	curent->letter = letter;
	curent->fix = 0;
	curent->next = NULL;
}

void			lst_new(t_form *start, char *str, int *n, char letter)
{
	t_form		*new;
	t_form		*tmp;

	if (start->tetris[0] == -1)
		filling_struct(start, str, letter);
	else
	{
		new = (t_form *)malloc(sizeof(t_form));
		filling_struct(new, str, letter);
		*n = *n + 1;
		tmp = start;
		if (start == NULL && new != NULL)
			start = new;
		else if (new != NULL)
		{
			while (tmp->next != NULL)
				tmp = tmp->next;
			tmp->next = new;
		}
	}
}
