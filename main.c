/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 17:30:39 by rvievill          #+#    #+#             */
/*   Updated: 2016/01/19 18:10:29 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

static void		change_char(char *str, char letter)
{
	int			i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
			str[i] = letter;
		i = i + 1;
	}
}

int				recover(int fd, int *ret)
{
	char		*str;
	char		letter;
	int			n;
	t_form		*new;

	letter = 'A';
	n = 1;
	new = (t_form *)malloc(sizeof(t_form));
	first_elem(new);
	str = (char *)malloc(sizeof(char) * BUF_SIZE + 1);
	if (str == NULL)
		return (0);
	while ((*ret = read(fd, str, 21)))
	{
		str[*ret] = '\0';
		if (error(str) == 1)
			return (1);
		change_char(str, letter++);
		lst_new(new, str, &n, letter);
	}
	if (str[20] == '\n' || str[0] == '\0')
		return (1);
	ft_solving(new, n);
	return (0);
}

int				main(int argc, char **argv)
{
	int			fd;
	int			ret;

	if (argc == 2)
	{
		fd = open(argv[1], O_RDONLY);
		if (recover(fd, &ret) == 1)
			ft_putstr("error\n");
	}
	else
		ft_putstr("error\n");
	return (0);
}
