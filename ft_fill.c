/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsoudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 15:51:54 by jsoudier          #+#    #+#             */
/*   Updated: 2016/01/22 15:57:03 by jsoudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_fill_rest(char grid[12][13], t_form *lst, t_limit pos)
{
	int k;
	int l;
	int c;

	k = 2;
	while (k < 8)
	{
		l = lst->tetris[k] + pos.l;
		c = lst->tetris[k + 1] + pos.c;
		if (l < pos.square && c < pos.square && grid[l][c] == '.')
			k = k + 2;
		else
			return (-1);
	}
	grid[pos.l][pos.c] = lst->letter;
	while (k > 2)
	{
		l = lst->tetris[k - 2] + pos.l;
		c = lst->tetris[k - 1] + pos.c;
		grid[l][c] = lst->letter;
		k = k - 2;
	}
	return (0);
}

int		ft_place(char grid[12][13], t_form *lst, t_limit pos, int start)
{
	while (pos.l < pos.square)
	{
		if (start != 1)
			pos.c = 0;
		start = 0;
		while (pos.c < pos.square)
		{
			if (grid[pos.l][pos.c] == '.')
			{
				lst->last_pos = (pos.l * pos.square) + pos.c;
				if (ft_fill_rest(grid, lst, pos) == -1)
				{
					pos.c++;
					return (ft_place(grid, lst, pos, 1));
				}
				else
					return (0);
			}
			pos.c++;
		}
		pos.l++;
	}
	return (-1);
}

t_form	*ft_last_placed(t_form *lst)
{
	t_form *begin;

	begin = lst;
	while (lst->next != NULL && (lst->next)->fix == 1)
		lst = lst->next;
	if (lst->next == NULL)
		return (NULL);
	return (lst);
}

t_form	*ft_re_fill(char grid[12][13], t_form *lst, t_limit *pos, t_form *begin)
{
	lst = ft_last_placed(begin);
	ft_clear(grid, lst, *pos);
	lst->fix = 0;
	(*pos).c = (lst->last_pos % (*pos).square) + 1;
	(*pos).l = lst->last_pos / (*pos).square;
	return (lst);
}

int		ft_fill(char grid[12][13], t_form *lst, t_limit pos, t_form *begin)
{
	int	placed;

	placed = ft_place(grid, lst, pos, 1);
	while (!(placed == 0 && lst->next == NULL))
	{
		pos.c = 0;
		pos.l = 0;
		if (placed == 0 && lst->next != NULL)
		{
			lst->fix = 1;
			lst = lst->next;
		}
		if (placed == -1 && lst->letter == 'A')
		{
			ft_clear(grid, lst, pos);
			pos.square++;
		}
		if (placed == -1 && lst->letter != 'A')
			lst = ft_re_fill(grid, lst, &pos, begin);
		placed = ft_place(grid, lst, pos, 1);
	}
	return (pos.square);
}
