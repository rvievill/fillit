/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 14:40:05 by rvievill          #+#    #+#             */
/*   Updated: 2016/01/19 18:04:38 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		nb_adj(char *str, int *nb, int i)
{
	int			sharp;

	sharp = 0;
	if (*str == '#' && *(str + 1) == '#' && (i + 1) < 21)
		sharp++;
	if (*str == '#' && *(str - 1) == '#' && (i - 1) >= 0)
		sharp++;
	if (*str == '#' && *(str + 5) == '#' && (i + 5) < 21)
		sharp++;
	if (*str == '#' && *(str - 5) == '#' && (i - 5) >= 0)
		sharp++;
	if (sharp == 3)
		*nb = *nb + 1;
	return (sharp);
}

int				error(char *str)
{
	int			i;
	int			nb_sharp;
	int			nb;

	i = 0;
	nb_sharp = 0;
	nb = 0;
	while (str[0] != '\0' && str[i])
	{
		if ((str[4] != '\n' || str[9] != '\n' || str[14] != '\n'
			|| str[19] != '\n') && (str[i] != '#' || str[i] != '.'))
			return (1);
		if (str[i] != '#' && str[i] != '.' && str[i] != '\n')
			return (1);
		if (str[i] == '#')
		{
			if (nb_adj(&str[i], &nb, i) == 2 || nb_adj(&str[i], &nb, i) == 3)
				nb = nb + 1;
			nb_sharp = nb_sharp + 1;
		}
		i++;
	}
	if (nb_sharp != 4 || nb < 2)
		return (1);
	return (0);
}
